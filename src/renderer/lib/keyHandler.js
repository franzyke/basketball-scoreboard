import {keys, shortcuts} from './shortcuts'
import store from '@/store'

export default (code) => {
  /* istanbul ignore else */
  if (keys.indexOf(code) >= 0) {
    let shortcut = shortcuts[code]
    switch (shortcut.type) {
      case 'team':
        store.dispatch('teams/setProperty', shortcut)
        break
    }
  }
}
