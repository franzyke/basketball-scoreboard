export const keys = ['1', '2', 'q', 'w', 'a', 's', 'z', 'x', 'e', 'r', 'z', 'x', 'd', 'f', 'Backspace']

export const shortcuts = {
  1: {
    property: 'score',
    value: 1,
    type: 'team',
    team: 'home',
    title: 'Home Score 1'
  },
  2: {
    property: 'score',
    value: 1,
    type: 'team',
    team: 'visitor',
    title: 'Visitor Score 1'
  },
  q: {
    property: 'score',
    value: 2,
    type: 'team',
    team: 'home',
    title: 'Home Score 1'
  },
  w: {
    property: 'score',
    value: 2,
    type: 'team',
    team: 'visitor',
    title: 'Visitor Score 1'
  },
  a: {
    property: 'score',
    value: 3,
    type: 'team',
    team: 'home',
    title: 'Home Score 1'
  },
  s: {
    property: 'score',
    value: 3,
    type: 'team',
    team: 'visitor',
    title: 'Visitor Score 1'
  },
  e: {
    property: 'score',
    value: -1,
    type: 'team',
    team: 'home',
    title: 'Visitor Score -1'
  },
  r: {
    property: 'score',
    value: -1,
    type: 'team',
    team: 'visitor',
    title: 'Visitor Score -1'
  },
  z: {
    property: 'fouls',
    value: 1,
    type: 'team',
    team: 'home',
    title: 'Home Foul +1'
  },
  x: {
    property: 'fouls',
    value: 1,
    type: 'team',
    team: 'visitor',
    title: 'Visitor Foul +1'
  },
  d: {
    property: 'fouls',
    value: -1,
    type: 'team',
    team: 'home',
    title: 'Home Foul +1'
  },
  f: {
    property: 'fouls',
    value: -1,
    type: 'team',
    team: 'visitor',
    title: 'Visitor Foul +1'
  },
  'Backspace': {
    property: 'shotclock',
    value: 24,
    type: 'board',
    title: 'Reset shotclock time'
  }
}
