// export default {
//   quarterDuration: 600, // not to be changed during game
//   foulsLimit: 4,
//   timeoutLimit: 4,
//   timeoutDuration: 10,
//   shotClockDuration: 24,
//   shotClock2Duration: 14,
//   gameId: null,
//   gameName: '3x3',
//   time: 600, // 900, // 900, 15 * 60 seconds , 15 minutes to be decremented every second
//   timeout: 30, // to be deceremented during timeout
//   homeTeam: 'Home',
//   visitorTeam: 'Visitor',
//   homeScore: 0,
//   visitorScore: 0,
//   shotclock: 24,
//   shotclock_2: 14,
//   quarter: 1,
//   homeFoul: 0,
//   visitorFoul: 0,
//   homeBonus: 0,
//   visitorBonus: 0,
//   ballPosession: 'home',
//   homeTOL: 3,
//   visitorTOL: 3,
//   playerFoul: 0,
//   homeScores: [],
//   visitorScores: [],
//   homeFouls: [],
//   visitorFouls: [],
// }

export default {
  time: 600,
  shotclock: 24,
  shotclock_2: 14
}
