const directories = require.context('./', true, /[./][a-z]+\/index.js$/)
const modules = {}

directories.keys().forEach(key => {
  modules[key.replace(/\/index.js/g, '').replace(/.\//g, '')] = directories(key).default
})

export default modules
