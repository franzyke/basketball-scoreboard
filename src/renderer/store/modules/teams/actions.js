export default {
  setProperty ({commit, rootGetters, getters}, prop) {
    commit('INCREMENT_TEAM_PROPERTY', prop)

    if (prop.property === 'fouls') {
      let currentTeamFoul = getters[prop.team].fouls
      // let foulLimit = rootGetters['settings/all'].foulLimit
      let foulLimit = 4

      if (currentTeamFoul >= foulLimit && currentTeamFoul < foulLimit + 2 && prop.value === 1) {
        let bonus = {
          property: 'bonus',
          team: prop.team,
          value: 1
        }
        commit('INCREMENT_TEAM_PROPERTY', bonus)
      }
      if (currentTeamFoul < foulLimit + 2 && prop.value === -1) {
        let bonus = {
          property: 'bonus',
          team: prop.team,
          value: -1
        }
        commit('INCREMENT_TEAM_PROPERTY', bonus)
      }
    }
  }
}
