export default {
  home (state) {
    return state.home
  },
  visitor (state) {
    return state.visitor
  }
}
