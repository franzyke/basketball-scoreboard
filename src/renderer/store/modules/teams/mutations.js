export default {
  INCREMENT_TEAM_PROPERTY (state, payload) {
    /* istanbul ignore else */
    if (state[payload.team][payload.property] + payload.value >= 0) {
      state[payload.team][payload.property] += payload.value
    }
  }
}
