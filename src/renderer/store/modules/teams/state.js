export default {
  home: {
    score: 0,
    fouls: 0,
    timeout: 0,
    tol: 3,
    bonus: 0
  },
  visitor: {
    score: 0,
    fouls: 0,
    timeout: 0,
    tol: 3,
    bonus: 0
  }
}
