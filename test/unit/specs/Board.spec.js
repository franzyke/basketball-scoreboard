import Vue from 'vue'
import Board from '@/components/Board'
import store from '@/store'
describe('Board.vue', () => {
  it('should render correct contents', () => {
    const vm = new Vue({
      el: document.createElement('div'),
      store,
      render: h => h(Board)
    }).$mount()
    // expect(vm.$store.getters['board/time']).to.equal(600)
    // expect(vm.$el.querySelector('.full-width').textContent).to.equal(vm.$store.getters['board/time'].toString())

    var homeScore = vm.$store.getters['teams/home'].score.toString()
    var e = document.createEvent('HTMLEvents')
    e.initEvent('keydown', true, true)
    e.keyCode = 49
    window.dispatchEvent(e)

    e.initEvent('keydown', true, true)
    e.keyCode = 8
    window.dispatchEvent(e)
    expect(vm.$store.getters['teams/home'].score).to.equal(parseInt(homeScore))
  })
})
