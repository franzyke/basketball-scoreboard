import Vue from 'vue'
import Board from '@/components/Board'
import store from '@/store'
import handleKey from '@/lib/keyHandler'

describe('Board.vue', () => {
  it('should handle keyboard input correctly huehue', () => {
    const vm = new Vue({
      el: document.createElement('div'),
      store,
      render: h => h(Board)
    }).$mount()

    var homeGetter = vm.$store.getters['teams/home']
    var visitorGetter = vm.$store.getters['teams/visitor']
    var homeScore = homeGetter.score.toString()
    var visitorScore = visitorGetter.score.toString()
    var homeFoul = homeGetter.fouls
    var visitorFoul = visitorGetter.fouls

    handleKey(';')
    expect(vm.$store.getters['teams/visitor'].score).to.equal(parseInt(visitorScore))
    expect(vm.$store.getters['teams/home'].score).to.equal(parseInt(homeScore))

    handleKey('1')
    expect(vm.$store.getters['teams/home'].score).to.equal(parseInt(homeScore) + 1)
    handleKey('e')
    expect(vm.$store.getters['teams/home'].score).to.equal(parseInt(homeScore))
    handleKey('q')
    expect(vm.$store.getters['teams/home'].score).to.equal(parseInt(homeScore) + 2)
    handleKey('a')
    expect(vm.$store.getters['teams/home'].score).to.equal(parseInt(homeScore) + 5)

    handleKey('2')
    expect(vm.$store.getters['teams/visitor'].score).to.equal(parseInt(visitorScore) + 1)
    handleKey('r')
    expect(vm.$store.getters['teams/visitor'].score).to.equal(parseInt(visitorScore))
    handleKey('w')
    expect(vm.$store.getters['teams/visitor'].score).to.equal(parseInt(visitorScore) + 2)
    handleKey('s')
    expect(vm.$store.getters['teams/visitor'].score).to.equal(parseInt(visitorScore) + 5)

    handleKey('z')
    handleKey('x')
    expect(homeGetter.fouls).to.equal(homeFoul + 1)
    expect(visitorGetter.fouls).to.equal(visitorFoul + 1)
    handleKey('d')
    handleKey('f')
    expect(homeGetter.fouls).to.equal(homeFoul)
    expect(visitorGetter.fouls).to.equal(visitorFoul)
  })

  it('should set time correctly', () => {
    const vm = new Vue({
      el: document.createElement('div'),
      store,
      render: h => h(Board)
    }).$mount()

    vm.$store.dispatch('game/setTime', 500)
    expect(vm.$store.getters['game/time']).to.equal(500)
  })
})
