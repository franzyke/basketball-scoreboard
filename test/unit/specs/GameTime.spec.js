import Vue from 'vue'
import store from '@/store'
import GameTime from '@/components/Board/GameTime'
// import TeamScore from '@/components/Board/TeamScore'
describe('GameTime.vue', () => {
  it('should render correct contents', () => {
    const vm = new Vue({
      el: document.createElement('div'),
      store,
      render: h => h(GameTime)
    }).$mount()

    expect(vm.$el.querySelector('.game-time').textContent).to.equal(vm.$store.getters['game/time'].toString())
  })
})
