import Vue from 'vue'
import store from '@/store'
import TeamScore from '@/components/Board/TeamScore'

describe('TeamScore.vue', () => {
  it('should render home team score', () => {
    const Constructor = Vue.extend(TeamScore, {store})
    const vm = new Constructor({
      store,
      propsData: {
        team: 'home'
      }
    }).$mount()
    expect(vm.$el.querySelector('.score.home').textContent).to.equal(vm.$store.getters['teams/home'].score.toString())
    expect(vm.score).to.equal(vm.$store.getters['teams/home'].score)
    expect(vm.team).to.equal('home')
  })

  it('should render visitor team score', () => {
    const Constructor = Vue.extend(TeamScore, {store})
    const vm = new Constructor({
      store,
      propsData: {
        team: 'visitor'
      }
    }).$mount()
    expect(vm.$el.querySelector('.score.visitor').textContent).to.equal(vm.$store.getters['teams/visitor'].score.toString())
    expect(vm.score).to.equal(vm.$store.getters['teams/visitor'].score)
    expect(vm.team).to.equal('visitor')
  })
})
